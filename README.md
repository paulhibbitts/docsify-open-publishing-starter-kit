# What's this Site About?

> This is a starter kit to create an open [Docsify](https://docsify.js.org) documentation site, with a link automatically created on each page to edit the source Markdown text in a GitHub or GitLab repository.

📸 Docsify Open Publishing Starter Kit Screenshot
---
![ Docsify Open Publishing Starter Kit](screenshot.jpg)
_Figure 1. Docsify Open Publishing Starter Kit. Explore a demo at [paulhibbitts.gitlab.io/docsify-open-publishing-starter-kit ](https://paulhibbitts.gitlab.io/docsify-open-publishing-starter-kit )_

🚀 Quick GitLab Pages Install Instructions
---
**Pre-flight Checklist**  

1. GitLab account

**Installation Steps**  

1. Fork this repository
2. Proceed with the steps described in the [Docsify GitLab Pages](https://docsify.js.org/#/deploy?id=gitlab-pages) documentation

Want to delete the fork dependency of your newly created repository?

1. Select **Settings**
2. Select **General**
3. Select **Advanced**
4. Scroll down to the bottom of the page and tap **Remove fork relationship**

📝 "Edit the Page on GitLab" Link Setup
---

1. Edit the file `index.html` in your repository
2. Find the sample GitLab repository URL `https://gitlab.com/paulhibbitts/docsify-open-publishing-starter-kit/-/tree/master/docs/` and replace it with your own repository URL, for example `https://gitlab.com/YourGitLabUsername/-/tree/master/Docsify/docs/`
3. Commit your changes.

💻 Editing your Docsify Site Locally
---  

1. Tap **Clone** on your repository page and copy the **Clone with HTTPS** address
2. Install/launch GitHub Desktop and choose **File** -> **Clone Repository**
3. Tap on the **URL** tab and paste the previously copied URL
4. Tap the **Clone** button
4. You will now be able to edit your Docsify site (in the `docs` folder) using the desktop editor of your choice (e.g. atom.io)
5. Use GitHub Desktop to push any changes to your repository. [Learn more about using GitHub Desktop](https://help.github.com/en/desktop/contributing-to-projects/committing-and-reviewing-changes-to-your-project).

You can also clone (i.e download) a copy of your repository to your computer and [run Docsify locally](https://docsify.js.org/#/quickstart) to preview your site. See the below video for details.

🧩 Embedding Docsify Page Content into Other Systems
---  

The optional ‘embedded’ (all lowercase) URL parameter hides a site’s sidebar and optional navbar for seamlessly embedding Docsify page content within another platform such as Canvas LMS, Moodle, Microsoft Teams etc.

To only display Docsify page content, add the following to a Docsify page URL:

`?embedded=true`

For example, https://paulhibbitts.gitlab.io/docsify-open-publishing-starter-kit/#/ would display a standard Docsify page while https://paulhibbitts.gitlab.io/docsify-open-publishing-starter-kit/#/?embedded=true would only display page content (i.e. no sidebar or optional navbar is shown).

📼 Video Walkthrough of Local Docsify Install/Config
---
[![Generating Documentation Sites with GitHub and Docsify - Alysson Alvaran](youtube.png)](https://www.youtube.com/watch?v=TV88lp7egMw)  
_Video 1. Generating Documentation Sites with GitHub and Docsify - Alysson Alvaran_

📚 Learn More about Docsify
---
[Docsify Documentation](https://docsify.js.org/#/?id=docsifyg)

🙇‍Credits and Special Thanks
---
[Docsify Themeable](https://github.com/jhildenbiddle/docsify-themeable)  
